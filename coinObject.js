function helper(x)
{
    let newElement;
    newElement = document.createElement("div");

    newElement.textContent = x;
    document.body.appendChild(newElement);
}
//============================================================================================
    const coin = {
    state: 0,
    flip: function() 
        {       
            this.state = Math.floor((Math.random() * 2));
        },
    //=====================================================
    toString: function() 
        {
            result = "";
            switch(this.state)
            {
                case 1:
                    result = "Heads";
                    break;
                case 0:
                    result = "Tails";
                    break;
            }
            return result;
        },
    //===============================================================
    toHTML: function() 
        {
            const image = document.createElement('img');

            switch(this.state)
            {
                case 1:
                    image.src = "./images/heads.jpg";
                    break;
                case 0:
                    image.src = "./images/tails.jpg";
                    break;
            }
            return image;
        }
};
//============================================
function display20Flips() 
{
// 4. One point: Use a loop to flip the coin 20 times, each time displaying the result of the flip as a string on the page.  After your loop completes, return an array with the result of each flip.

    const results = [];
    helper("They are each strings: ")
    for(let loopCount = 0; loopCount < 20; loopCount++)
    {
        coin.flip();
        
        helper(coin.toString());
        results.push(coin.state);
    }
    helper("This is Array: " + results)
}

function display20Images() 
{
// 5. One point: Use a loop to flip the coin 20 times, and display the results of each flip as an image on the page.  After your loop completes, return an array with result of each flip.

    const results = [];
    helper("They are images: ")
    for(let loopCount = 0; loopCount < 20; loopCount++)
    {
        coin.flip();
        
        document.body.appendChild(coin.toHTML());
        results.push(coin.state);
    }
    helper("This is Array: " + results)
}
//===================================================================

display20Flips();
display20Images();
